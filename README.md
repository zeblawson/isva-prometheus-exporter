# ISVA Prometheus Exporter

This repository provides a Prometheus monitoring exporter for IBM© Security Verify Access and Security Access Manager, supporting both virtual and physical appliance form factors.

## Build Instructions

### Python

#### Prerequisites

- Python 3.4+

#### Steps

1. Clone this repository

    ```bash
    git clone --branch master https://gitlab.com/zeblawson/isva-prometheus-exporter.git
    cd isva-prometheus-exporter
    ```

2. *(Optional)* Create a Virtual Environment

    ```bash
    python3 -m venv venv
    source venv/bin/activate
    ```

3. Install Required Packages

    ```bash
    pip install -r requirements.txt
    ```

4. Run

    ```bash
   python .\exporter.py --help
   usage: exporter.py [-h] [--address ADDRESS] [--port PORT] [--timeout] [--verify] [--debug] [--log LOG] [--console-address https://...] [--console-username] [--console-password] [--runtime-address http(s)://...] [--runtime-username] [--runtime-password] [--pd-username sec_master] [--pd-password]
                      [--pd-domain Default] [--collectors collectors.yaml]
   
   options:
     -h, --help            show this help message and exit
     --address ADDRESS     Exporter bind address
     --port PORT           Exporter bind port
     --timeout             Maximum time for individual requests in seconds
     --verify              Certificate trust store for HTTPS
     --debug               Enable debug logging
     --log LOG             Logging output file. Default is standard output
     --collectors collectors.yaml
                           Custom collectors YAML file
   
   Console endpoint:
     --console-address https://...
                           LMI console address
     --console-username    LMI console runtime BA username
     --console-password    LMI console runtime BA username
   
   Runtime endpoint:
     --runtime-address http(s)://...
                           AAC Java runtime address
     --runtime-username    AAC Java runtime BA username
     --runtime-password    AAC Java runtime BA password
   
   Policy domain:
     --pd-username sec_master
                           TAM username
     --pd-password         TAM password
     --pd-domain Default   TAM domain
   ``` 
   
   ```bash
   python .\exporter.py --console-address https://192.168.37.128 --console-password ****** --runtime-address https://192.168.37.129 --pd-password ****** --collectors custom.yaml
   ```   

### Docker Image

#### Steps

1. Build the Docker image

    ```bash
    docker build . -t isva/isva-prometheus-exporter
    ```

2. Run the container

    ```bash
    docker run -e CS_ADDR='https://192.168.37.128' -e CS_PASS='******' -e RT_ADDR='https://192.168.37.129' -e PD_PASS='******' -e DEBUG=1 -p 9020:9020 isva/isva-prometheus-exporter
    ```

## Command Line and Environment Arguments 

| Environment Variable | Command Line       | Default    | Description                                                                                           |
|----------------------|--------------------|------------|-------------------------------------------------------------------------------------------------------|
| TIMEOUT              | --timeout          | 10         | Maximum time for individual requests in seconds. Applies to both console and runtime endpoints        |
| VERIFY               | --verify           |            | Certificate trust store for https://. Omit to disable SSL verification                                |
| CS_ADDR              | --console-address  |            | LMI console address                                                                                   |
| CS_USER              | --console-username | admin      | LMI console username                                                                                  |
| CS_PASS              | --console-password |            | LMI console password                                                                                  |
| RT_ADDRESS           | --runtime-address  |            | AAC Java runtime address. Either http:// or https://                                                  |
| RT_USER              | --runtime-username |            | AAC Java runtime username. Generally not required.                                                    |
| RT_PASS              | --runtime-password |            | AAC Java runtime username. Generally not required.                                                    |
| PD_DOMAIN            | --pd-domain        | Default    | Policy domain                                                                                         |
| PD_USER              | --pd-username      | sec_master | Policy username                                                                                       |
| PD_PASS              | --pd-password      |            | Policy password                                                                                       |
| COLLECTORS           | --collectors       |            | Custom collectors YAML file path. See **Collectors** below for format. Omit to use default collectors |
| DEBUG                | --debug            |            | Enable debug logging                                                                                  |
| LOG                  | --log              |            | Logging output file. Omit to send standard output                                                     |

For collectors to be enabled for a particular endpoint type, the following user-supplied parameters must be supplied in addition to the defaults:  
- For console collection, provide an address and password.
- For runtime collection, provide an address. Generally, no username or password is required for accessing this endpoint. The advanced tuning parameter `runtime_profile.enable.monitor` must be set to the value `true`. See https://community.ibm.com/community/user/security/blogs/leo-farrell/2018/06/10/monitoring-federation-advanced-access
- For policy director collection, specify a password.

## Collectors

By not specifying a custom collectors file the following collectors are enabled by default `uptime, cpu, memory, storage, interface-throughput, runtime-jvm, runtime-threads, runtime-database`. These can be found in the `defaults.yml` file. 

It may often be the case a custom collectors file is required. For reasons of high appliance cardinality, prolonged runtime of collectors (see `caching`) or resource demands on the appliance a collectors file enables more controls and specific collectors.   

When specifying a collectors entry in the file, the following optional parameters are always available:

- `enabled`: a boolean to enable or disable this collector. Collectors are enabled by default.
- `caching`: a boolean to enable or disable caching. Caching can be useful in long-running collections that exceed the Prometheus `scrape_interval` or `scrape_timeout`. A cached collector runs outside the assigned collection thread on a dedicated and shared thread. Metrics returned will always be the last collected. Ensure you increase the timeout to match the anticipated duration. Caching is disabled by default.
 
Some collectors are noted as being _core_ collectors. These cannot be disabled, cached or other respecified in a custom collectors file. 

### Up

Retrieves the up down state of the endpoint. This is a core collector and will always be enabled for console and runtime endpoints. It cannot be respecified in YAML. 

**Endpoint**: console, runtime 

**Exported Metrics Example**
```html
# HELP isva_console_up Indicates whether the console is up or down.
# TYPE isva_console_up gauge
isva_console_up 1.0
# HELP isva_runtime_up Indicates whether the runtime is up or down.
# TYPE isva_runtime_up gauge
isva_runtime_up 1.0
```

### Build

Retrieves some basic build information about the appliance. This is a core collector and will _always_ be enabled for a valid console endpoint. It cannot be respecified in YAML. 

**Endpoint**: console

**Exported Metrics Example**
```html
# HELP isva_build_info Appliance build information
# TYPE isva_build_info gauge
isva_build_info{hostname="isam1",version="10.0.0.0"} 1.0
```

### Uptime

Retrieves the appliance uptime duration in seconds. 

**Endpoint**: console, runtime (optional)

**YAML Format Example**:
```yaml
---
uptime:  
```
**Exported Metrics Example**
```html
# HELP isva_appliance_uptime_seconds_total Appliance uptime in seconds
# TYPE isva_appliance_uptime_seconds_total counter
isva_appliance_uptime_seconds_total 852840.0
# HELP isva_runtime_uptime_seconds_total Runtime uptime in seconds
# TYPE isva_runtime_uptime_seconds_total counter
isva_runtime_uptime_seconds_total 39.0
```

### CPU

Provides appliance CPU usage ratios for user, idle, and system modes.
  

**Endpoint**: console

**YAML Format Example**:
```yaml
---
cpu:  
```
**Exported Metrics Example**
```html
# HELP isva_cpu_usage_ratio Ratios of CPU time spent in each mode.
# TYPE isva_cpu_usage_ratio gauge
isva_cpu_usage_ratio{mode="idle"} 0.8834000000000001
isva_cpu_usage_ratio{mode="system"} 0.0467
isva_cpu_usage_ratio{mode="user"} 0.0699
```

### Memory

Provides appliance memory used, free, and total aspects in bytes.

**Endpoint**: console

**YAML Format Example**:
```yaml
---
memory:  
```
**Exported Metrics Example**
```html
# HELP isva_memory_used_bytes Memory used in bytes.
# TYPE isva_memory_used_bytes gauge
isva_memory_used_bytes 1.7206083584e+09
# HELP isva_memory_free_bytes Memory free in bytes.
# TYPE isva_memory_free_bytes gauge
isva_memory_free_bytes 2.5057296384e+09
# HELP isva_memory_size_bytes Memory size in bytes.
# TYPE isva_memory_size_bytes gauge
isva_memory_size_bytes 4.2263379968e+09
```

### Storage

Provides appliance storage used, free, and total aspects in bytes for both root and boot partitions.

**Endpoint**: console

**YAML Format Example**:
```yaml
---
storage:  
```
**Exported Metrics Example**
```html
# HELP isva_partition_free_bytes Partition free space in bytes
# TYPE isva_partition_free_bytes gauge
isva_partition_free_bytes{partition="boot"} 6.6584576e+07
isva_partition_free_bytes{partition="root"} 4.54923649024e+09
# HELP isva_partition_used_bytes Partition used space in bytes
# TYPE isva_partition_used_bytes gauge
isva_partition_used_bytes{partition="boot"} 1.3709082624e+08
isva_partition_used_bytes{partition="root"} 2.78015246336e+09
# HELP isva_partition_size_bytes Partition size in bytes.
# TYPE isva_partition_size_bytes gauge
isva_partition_size_bytes{partition="root"} 7.3293889536e+09
isva_partition_size_bytes{partition="boot"} 2.0367540224e+08
```

### Interface Throughput

Provides the average number of inbound or outbound bytes for each ethernet attached to an interface. 

**Endpoint**: console

**Parameters**

 * `interfaces` (optional) specify which interfaces to include (Ex. 1.1). Accepts either a single regular expression or an explicit list of interface names. Defaults to `.*` if omitted. 

**YAML Format Examples**:

```yaml
---
interface-throughput:  
```

```yaml
---
interface-throughput:
  interfaces: 1\.[1-3] # Interfaces 1.1, 1.2 and 1.3 as a pattern  
   
```

```yaml
---
interface-throughput:
  # Interfaces 1.1, 1.2 and 1.3 as a list
  interfaces:
     - 1.1 
     - 1.2
     - 1.3
   
```

**Exported Metrics Example**
```html
# HELP isva_interface_throughput_in_bytes The average number of inbound bytes per second recorded for an interface.
# TYPE isva_interface_throughput_in_bytes gauge
isva_interface_throughput_in_bytes{ethernet="eth0",interface="1.1"} 0.0
isva_interface_throughput_in_bytes{ethernet="eth1",interface="1.2"} 0.0
# HELP isva_interface_throughput_out_bytes The average number of outbound bytes per second recorded for an interface.
# TYPE isva_interface_throughput_out_bytes gauge
isva_interface_throughput_out_bytes{ethernet="eth0",interface="1.1"} 0.0
isva_interface_throughput_out_bytes{ethernet="eth1",interface="1.2"} 0.0
```

### Proxy Health

Provides a summary of reverse proxy and associated junction health states. These health states are denoted by integers: 0 for healthy, 1 for unhealthy, and 2 for warning states.
 
**Endpoint**: console

**YAML Format Example**:

```yaml
---
proxy-health:  
```

**Exported Metrics Example**
```html
# HELP isva_proxy_health Summary of reverse proxy health
# TYPE isva_proxy_health gauge
isva_proxy_health{proxy="orange"} 0.0
isva_proxy_health{proxy="banana"} 0.0
isva_proxy_health{proxy="apple"} 2.0
# HELP isva_junction_health Summary of reverse proxy junction health
# TYPE isva_junction_health gauge
isva_junction_health{junction="/",proxy="orange"} 0.0
isva_junction_health{junction="/",proxy="banana"} 0.0
isva_junction_health{junction="@app",proxy="apple"} 1.0
isva_junction_health{junction="/special",proxy="apple"} 1.0
isva_junction_health{junction="/",proxy="apple"} 0.0
isva_junction_health{junction="/mga",proxy="apple"} 0.0
```

### Proxy Throughput

Provides the latest interval summary of reverse proxy throughput records. Intervals are refreshed every whole 10 minute periods (Ex. 10:20 to 10:30, 23:50 to 00:00) 

**Endpoint**: console

**Parameters**

 * `proxies` (optional) specify which proxies to include. Accepts either a single regular expression or an explicit list of proxy names. Defaults to `.*` if omitted. 


**YAML Format Example**:

```yaml
---
proxy-throughput:
```

```yaml
---
proxy-throughput:
  proxies:
    - apple
    - banana
```

```yaml
---
proxy-throughput:
  proxies: (app|ora).*

```

**Exported Metrics Example**
```html
# HELP isva_proxy_throughput Latest summary of reverse proxy throughput records
# TYPE isva_proxy_throughput gauge
isva_proxy_throughput{proxy="orange"} 0.0
isva_proxy_throughput{proxy="banana"} 0.0
isva_proxy_throughput{proxy="apple"} 33.0
```

### Proxy Traffic

Provides the latest interval summary of reverse proxy useragent or junction traffic records. Intervals are refreshed every whole 10 minute periods (Ex. 10:20 to 10:30, 23:50 to 00:00) 

**Endpoint**: console

**Parameters**

 * `proxies` (optional) specify which proxies to include. Accepts either a single regular expression or an explicit list of proxy names. Defaults to `.*` if omitted. 
 * `aspects` (optional) specify which aspects to include. Accepts a list accept values `useragent` and `junction`. Defaults to both aspects if omitted.


**YAML Format Example**:

```yaml
---
proxy-traffic:
```

```yaml
---
proxy-traffic:
  proxies:
    - apple
    - banana
```

```yaml
---
proxy-traffic:
  proxies: (ban|ora).*
  aspects:
     - junction
```

```yaml
---
proxy-traffic:
  proxies: 
     - orange
  aspects:
     - useragent
     - junction
```

**Exported Metrics Example**
```html
# HELP isva_proxy_junction_traffic Latest summary of reverse proxy traffic count by junction
# TYPE isva_proxy_junction_traffic gauge
isva_proxy_junction_traffic{junction="/",proxy="banana"} 35.0
isva_proxy_junction_traffic{junction="/sso",proxy="banana"} 31.0
isva_proxy_junction_traffic{junction="/",proxy="apple"} 45.0
isva_proxy_junction_traffic{junction="/sso",proxy="apple"} 33.0
# HELP isva_proxy_useragent_traffic Latest summary of reverse proxy traffic count by useragent
# TYPE isva_proxy_useragent_traffic gauge
isva_proxy_useragent_traffic{proxy="banana",useragent="FIREFOX"} 66.0
isva_proxy_useragent_traffic{proxy="apple",useragent="FIREFOX"} 78.0
```

### Junction Average Response Time

Provides the latest interval summary of reverse proxy junction average response time in seconds. Intervals are refreshed every whole 10 minute periods (Ex. 10:20 to 10:30, 23:50 to 00:00) 

**Endpoint**: console

**Parameters**

 * `proxies` (optional) specify which proxies to include. Accepts either a single regular expression or an explicit list of proxy names. Defaults to `.*` if omitted. 
 
**YAML Format Example**:

```yaml
---
junction-average-response-time:
```

```yaml
---
junction-average-response-time:
  proxies:
    - apple
    - banana
```

```yaml
---
junction-average-response-time:
  proxies: (ban|ora).*  
```

**Exported Metrics Example**
```html
# HELP isva_junction_average_response_time Latest summary of reverse proxy junction response time in seconds
# TYPE isva_junction_average_response_time gauge
isva_junction_average_response_time{junction="/",proxy="banana"} 13.0
isva_junction_average_response_time{junction="/mga",proxy="banana"} 9.0
isva_junction_average_response_time{junction="/",proxy="apple"} 3.0
isva_junction_average_response_time{junction="/sso",proxy="apple"} 11.0
```

### Runtime Database

Provides the connection pool counters for runtime `config` or `hvdb` JDBC data sources.

**Endpoint**: runtime

**Parameters**

* `databases` (optional) specify which databases to include. Accepts a list accept values `hvdb` or `config`. Defaults to both if omitted. 
 
**YAML Format Example**:

```yaml
---
runtime-database:
```

```yaml
---
runtime-database:
  databases:
    - config
```

**Exported Metrics Example**
```html
# HELP isva_runtime_database_count Connection pool counters for runtime JDBC data sources.
# TYPE isva_runtime_database_count gauge
isva_runtime_database_count{counter="CreateCount",database="config"} 152.0
isva_runtime_database_count{counter="DestroyCount",database="config"} 152.0
isva_runtime_database_count{counter="FreeConnectionCount",database="config"} 0.0
isva_runtime_database_count{counter="ConnectionHandleCount",database="config"} 0.0
isva_runtime_database_count{counter="WaitTime",database="config"} 0.0
isva_runtime_database_count{counter="ManagedConnectionCount",database="config"} 0.0
```

### Runtime JVM

Provides the JVM heap free, used and total aspect in bytes, number of GC cycles and time taken in GC.

**Endpoint**: runtime

 
**YAML Format Example**:

```yaml
---
runtime-jvm:
```

**Exported Metrics Example**
```html
# HELP isva_runtime_heap_used_bytes Runtime JVM heap used space in bytes.
# TYPE isva_runtime_heap_used_bytes gauge
isva_runtime_heap_used_bytes 5.77943902355456e+014
# HELP isva_runtime_heap_free_bytes Runtime JVM heap available space in bytes.
# TYPE isva_runtime_heap_free_bytes gauge
isva_runtime_heap_free_bytes 1.673855911329792e+015
# HELP isva_runtime_heap_size_bytes Runtime JVM heap size in bytes.
# TYPE isva_runtime_heap_size_bytes gauge
isva_runtime_heap_size_bytes 2.251799813685248e+015
# HELP isva_runtime_garbage_collection_count_total Runtime JVM number of garbage collection cycles.
# TYPE isva_runtime_garbage_collection_count_total counter
isva_runtime_garbage_collection_count_total 48.0
# HELP isva_runtime_garbage_collection_time_total Runtime JVM total time taken performing garbage collection cycles.
# TYPE isva_runtime_garbage_collection_time_total counter
isva_runtime_garbage_collection_time_total 763.0
```

### Runtime Threads

Provides the runtime JVM thread usage.

**Endpoint**: runtime
 
**YAML Format Example**:

```yaml
---
runtime-threads:
```

**Exported Metrics Example**
```html
# HELP isva_runtime_thread_count Runtime JVM thread usage
# TYPE isva_runtime_thread_count gauge
isva_runtime_thread_count{counter="ActiveThreads"} 2.0
isva_runtime_thread_count{counter="PoolSize"} 4.0
```

### PD Stats

Provides ...

**Endpoint**: console

**Parameters**

* `servers` (optional): Specifies which proxies to include. Accepts either a single regular expression or an explicit list of proxy names. Server names of interest are often in the format `<proxy-name>-webseald-<hostname>`. Defaults to `.*` if omitted.
* `components` (optional): Specifies which statistical components to collect. Accepts either a single regular expression or an explicit list of component names. Defaults to `pdweb.threads` if omitted.
 
   Some common components are:
  * `pdweb.authn`
  * `pdweb.authz`
  * `pdweb.doccache`
  * `pdweb.http`
  * `pdweb.https`
  * `pdweb.jct` (`pdweb.vhj` component statistics are rolled into this when included. Do not specify a trailing digit)
  * `pdweb.jmt`
  * `pdweb.sescache`
  * `pdweb.threads` 


**YAML Format Example**:

```yaml
---
pd-stats:
```

```yaml
---
pd-stats:
  servers:
    - apple-webseald-isam1
    - banana-webseald-isam1
  components:
    - pdweb.threads
```

```yaml
---
pd-stats:
  servers: .*(ban|ora).*  
```

**Exported Metrics Example**
```html
# HELP isva_pdweb_threads The pdweb.threads statistics component gathers information about WebSEAL worker thread activity
# TYPE isva_pdweb_threads gauge
isva_pdweb_threads{counter="active",server="banana-webseald-isam1"} 0.0
isva_pdweb_threads{counter="total",server="banana-webseald-isam1"} 300.0
isva_pdweb_threads{counter="'default' active",server="banana-webseald-isam1"} 0.0
isva_pdweb_threads{counter="'default' total",server="banana-webseald-isam1"} 300.0
isva_pdweb_threads{counter="active",server="apple-webseald-isam1"} 0.0
isva_pdweb_threads{counter="total",server="apple-webseald-isam1"} 300.0
isva_pdweb_threads{counter="'default' active",server="apple-webseald-isam1"} 0.0
isva_pdweb_threads{counter="'default' total",server="apple-webseald-isam1"} 300.0
```

## Feedback

Welcome any feedback, suggestions, or issues you may encounter while using this project. 

You can submit your feedback by:

- Creating a [GitHub issue](https://gitlab.com/zeblawson/isva-prometheus-exporter/-/issues) for any bugs or feature requests.
- Emailing us directly at [zeblawson@gmail.com](mailto:zeblawson@gmail.com) for general inquiries or feedback.


## License 

This package is licensed under the Apache 2.0 License. See _LICENSE_