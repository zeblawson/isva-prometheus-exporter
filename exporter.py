#!/usr/bin/env python3
import argparse
import os
import prometheus_client
import time

from collector import *
from collector.loader import *
from collector.logging import *

from collector.pd import *

DEFAULT_BIND_PORT = 10017
DEFAULT_BIND_ADDRESS = '0.0.0.0'


def run(registry, address, port):
    prometheus_client.start_http_server(addr=address, port=port, registry=registry)
    logger.info(ic.format(address, port))
    while True:
        time.sleep(1)


def bail(code=1, message=None):
    logger.error(ic.format(code, message))
    if message:
        sys.stderr.write(repr(message))
    sys.exit(code)


def overwrite(environment, arguments, argument):
    if os.environ.get(environment):
        return os.environ.get(environment)
    elif arguments.get(argument):
        return arguments.get(argument)


def endpoints(arguments):
    ic()
    ic(arguments)
    timeout, verify = (
        overwrite('TIMEOUT', arguments, 'timeout'),
        overwrite('VERIFY', arguments, 'verify')
    )
    console, runtime, pd = None, None, None
    address, username, password = (
        overwrite('CS_ADDR', arguments, 'console-address'),
        overwrite('CS_USER', arguments, 'console-username'),
        overwrite('CS_PASS', arguments, 'console-password')
    )
    ic(address, username, password)
    if address and username and password:
        console = Endpoint(
            address,
            username=username,
            password=password,
            verify=verify,
            timeout=timeout,
        )
    address, username, password = (
        overwrite('RT_ADDR', arguments, 'runtime-address'),
        overwrite('RT_USER', arguments, 'runtime-username'),
        overwrite('RT_PASS', arguments, 'runtime-password')
    )
    if address:
        runtime = Endpoint(
            address,
            username=username,
            password=password,
            verify=verify,
            timeout=timeout,
        )
    domain, username, password = (
        overwrite('PD_DOM', arguments, 'pd-domain'),
        overwrite('PD_USER', arguments, 'pd-username'),
        overwrite('PD_PASS', arguments, 'pd-password')
    )
    if username and password and console:
        pd = Pd(password, username=username, domain=domain)
    return console, runtime, pd


def parse():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument(
        '--address', help='Exporter bind address', default=DEFAULT_BIND_ADDRESS
    )
    parser.add_argument('--port', help='Exporter bind port', default=DEFAULT_BIND_PORT)
    parser.add_argument('--timeout', metavar='', help='Maximum time for individual request', default=10)
    parser.add_argument('--verify', action='store_true', help='Certificate trust store for HTTPS', default=False)
    parser.add_argument('--debug', action='store_true', help='Enable debug logging')
    parser.add_argument('--log', help='Logging output file. Default is standard output')
    console = parser.add_argument_group('Console endpoint')
    console.add_argument(
        '--console-address', dest='console-address', metavar='https://...', help='LMI console address'
    )
    console.add_argument(
        '--console-username', dest='console-username', metavar='', default='admin',
        help='LMI console runtime BA username'
    )
    console.add_argument(
        '--console-password', dest='console-password', metavar='', help='LMI console runtime BA username'
    )
    runtime = parser.add_argument_group('Runtime endpoint')
    runtime.add_argument(
        '--runtime-address', dest='runtime-address', metavar='http(s)://...', help='AAC Java runtime address'
    )
    runtime.add_argument(
        '--runtime-username', dest='runtime-username', metavar='', help='AAC Java runtime BA username'
    )
    runtime.add_argument(
        '--runtime-password', dest='runtime-password', metavar='', help='AAC Java runtime BA password'
    )
    pd = parser.add_argument_group('Policy domain')
    pd.add_argument(
        '--pd-username', dest='pd-username', metavar='sec_master', default='sec_master', help='TAM username'
    )
    pd.add_argument('--pd-password', dest='pd-password', metavar='', help='TAM password')
    pd.add_argument(
        '--pd-domain', dest='pd-domain', metavar='Default', default='Default', help='TAM domain'
    )
    parser.add_argument(
        '--collectors', dest='collectors', metavar='collectors.yaml', help='Custom collectors YAML file'
    )
    return parser.parse_args().__dict__


def main():
    try:
        arguments = parse()
        prepare(
            debug=overwrite('DEBUG', arguments, 'debug'),
            filename=overwrite('LOG', arguments, 'log')
        )
        console, runtime, pd = endpoints(arguments)
        logger.info(ic.format(console, runtime, pd))
        if arguments['collectors']:
            path = Path(arguments['collectors'])
        else:
            path = Path(__file__).parent.joinpath('defaults.yaml')
        logger.info(ic.format(path))
        collectors = load(path, console, runtime, pd)
        logger.info(ic.format(collectors))
        registry = prometheus_client.CollectorRegistry()
        for collector in collectors:
            registry.register(collector)
        if registry:
            run(registry, arguments['address'], arguments['port'])
    except YamlValidationError as error:
        bail(error.errors)


if __name__ == '__main__':
    main()
