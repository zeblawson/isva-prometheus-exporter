from icecream import ic
from prometheus_client.metrics_core import GaugeMetricFamily
from . import BaseCollector, matches

DOCUMENTATION = {
    'pdweb.doccache': 'The pdweb.doccache statistics component gathers information about WebSEAL document-caching '
                      'activity',
    'pdweb.jmt': 'The pdweb.jmt statistics component gathers information about the WebSEAL junction mapping table',
    'pdweb.threads': 'The pdweb.threads statistics component gathers information about WebSEAL worker thread activity',
    'pdweb.jct': 'Statistics around configured junctions'
}


class Pd:
    def __init__(self, password, username='sec_master', domain='Default'):
        self.password = password
        self.username = username
        self.domain = domain

    def __repr__(self):
        return f'Pd({self.username}@{self.domain})'


class PdCollector(BaseCollector):

    def __init__(self, endpoint, pd, facts, **kwargs):
        super().__init__(endpoint, facts, **kwargs)
        self.pd = pd

    def post(self, command):
        data = {
            'admin_id': self.pd.username,
            'admin_pwd': self.pd.password,
            'admin_domain': self.pd.domain,
            'commands': [
                command,
            ],
        }
        body = self.endpoint.post('/isam/pdadmin', data)
        if body:
            results = body.get('result').splitlines()[1:]
            results = list(map(str.strip, results))
            return results


class PdFactCollector(PdCollector):

    def update(self):
        results = self.post(f'server list')
        if results:
            self.facts['servers'] = results
        return []


class PdStatsCollector(PdCollector):
    def __init__(self, endpoint, pd, facts, servers=r'.*', components=None, **kwargs):
        super().__init__(endpoint, pd, facts, **kwargs)
        if components is None:
            components = ['pdweb.threads']
        self.pd = pd
        self.servers = servers
        self.components = components

    def merge(self, server, results, metrics):
        component, metric, junction = None, None, None
        for line in results:
            line = line.strip()
            if not line:
                continue
            if 'HPDRA' in line:
                component = None
                metric = None
                junction = None
            elif line[0] == '[':
                junction = line[1:-1]
                if junction[0] != '/':
                    junction = '@' + junction
                ic(junction)
            elif ':' not in line:  # A new component?
                component = line
                if component.startswith('pdweb.jct') or component.startswith('pdweb.vhj'):
                    component = 'pdweb.jct'
                if component not in metrics:
                    label = self.label(f'{component}')
                    label = label.replace('.', '_')
                    if component in ('pdweb.jct', 'pdweb.vhj'):
                        metric = GaugeMetricFamily(label, DOCUMENTATION.get(component, ''),
                                                   labels=['server', 'junction', 'counter'])
                    else:
                        metric = GaugeMetricFamily(label, DOCUMENTATION.get(component, ''),
                                                   labels=['server', 'counter'])
                    metrics[component] = metric
                metric = metrics[component]
            elif ':' in line and component:
                key, value = line.split(':', 1)
                key = key.strip()
                value = value.strip()
                if metric:
                    if component in ('pdweb.jct', 'pdweb.vhj'):
                        metric.add_metric([server, junction, key], value)
                    else:
                        metric.add_metric([server, key], value)

    def update(self):
        metrics = {}

        combined = {}
        for server in self.matches('servers', self.servers):
            ic(server)
            results = self.post(f'server task {server} stats get')
            if not results:
                continue
            ic(results)
            self.merge(server, results, metrics)
        return [metrics[key] for key in matches(self.components, metrics.keys())]


__all__ = ('Pd', 'PdFactCollector', 'PdStatsCollector')
