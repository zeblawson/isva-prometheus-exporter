from . import *
from prometheus_client.metrics_core import GaugeMetricFamily, CounterMetricFamily


class RuntimeDatabaseCollector(BaseCollector):
    def __init__(self, endpoint, facts, databases=None, **kwargs):
        super().__init__(endpoint, facts, **kwargs)
        if databases is None:
            databases = ['hvdb', 'config']
        self.databases = databases

    def update(self):
        metrics = []
        metric = GaugeMetricFamily(
            self.label('runtime_database_count'),
            'Connection pool counters for runtime JDBC data sources.',
            labels=['database', 'counter']
        )
        for database in self.databases:
            body = self.endpoint.get(f'/monitor/{database}')
            if body:
                body.pop('Datasource')
                for counter, count in body.items():
                    metric.add_metric([database, counter], count)
        if metric.samples:
            metrics.append(metric)
        return metrics


class RuntimeThreadsCollector(BaseCollector):
    def update(self):
        body = self.endpoint.get(f'/monitor/threads')
        if not body:
            return []
        metric = GaugeMetricFamily(
            self.label('runtime_thread_count'),
            'Runtime JVM thread usage',
            labels=['counter']
        )
        for key, value in body.items():
            metric.add_metric([key], value)
        return [metric]


class RuntimeJvmCollector(BaseCollector):
    def update(self):
        metrics = []
        body = self.endpoint.get(f'/monitor/jvm')
        if not body:
            return metrics
        metrics.append(
            GaugeMetricFamily(
                self.label('runtime_heap_used_bytes'),
                'Runtime JVM heap used space in bytes.',
                value=convert(body, 'UsedMemory')
            )
        )
        metrics.append(
            GaugeMetricFamily(
                self.label('runtime_heap_free_bytes'),
                'Runtime JVM heap available space in bytes.',
                convert(body, 'FreeMemory')
            )
        )
        metrics.append(
            GaugeMetricFamily(
                self.label('runtime_heap_size_bytes'),
                'Runtime JVM heap size in bytes.',
                value=convert(body, 'Heap')
            )
        )
        metrics.append(
            CounterMetricFamily(
                self.label('runtime_garbage_collection_count'),
                'Runtime JVM number of garbage collection cycles.',
                int(body['GcCount'])
            )
        )
        metrics.append(
            CounterMetricFamily(
                self.label('runtime_garbage_collection_time'),
                'Runtime JVM total time taken performing garbage collection cycles.',
                int(body['GcTime'])
            )
        )
        return metrics


__all__ = ['RuntimeJvmCollector', 'RuntimeThreadsCollector', 'RuntimeDatabaseCollector']
