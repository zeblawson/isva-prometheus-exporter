import re
import time

from datetime import datetime, timedelta
from icecream import ic
from prometheus_client.metrics_core import GaugeMetricFamily, InfoMetricFamily, CounterMetricFamily
from . import *


class ConsoleFactCollector(BaseCollector):
    def update(self):
        # Interfaces
        body = self.endpoint.get('/net/ifaces')
        if not body:
            raise RequiredFactException('interfaces')
        self.facts['interfaces'] = [
            interface['label'] for interface in body.get('interfaces', [])
        ]
        # Proxies
        body = self.endpoint.get('/wga/reverseproxy')
        if not body:
            self.facts['proxies'] = []
        else:
            self.facts['proxies'] = [
                proxy['instance_name'] for proxy in body
            ]
        ic(self.facts)
        # Version
        body = self.endpoint.get('/core/sys/versions')
        version, hostname = '?', '?'

        if body:
            version = body.get('firmware_version')
        body = self.endpoint.get('/net/general')
        # Hostname
        if body:
            hostname = body.get('hostName')
        build = {'version': version, 'hostname': hostname}
        self.facts.update(build)
        return [InfoMetricFamily(self.label('build'), 'Appliance build information', value=build)]


class CpuCollector(BaseCollector):
    def update(self):
        body = self.endpoint.get('/statistics/systems/cpu.json?timespan=1d')
        if not body:
            return []
        metric = GaugeMetricFamily(
            self.label('cpu_usage_ratio'),
            'Ratios of CPU time spent in each mode.',
            labels=['mode'],
        )
        for mode in ['idle', 'system', 'user']:
            metric.add_metric([mode], float(body[mode + '_cpu']) / 100.0)
        return [metric]


class MemoryCollector(BaseCollector):
    def update(self):
        body = self.endpoint.get('/statistics/systems/memory.json?timespan=1d')
        if not body:
            return []
        metrics = [GaugeMetricFamily(
            self.label('memory_used_bytes'),
            'Memory used in bytes.',
            value=convert(body, 'used')
        ), GaugeMetricFamily(
            self.label('memory_free_bytes'),
            'Memory free in bytes.',
            value=convert(body, 'free')
        ), GaugeMetricFamily(
            self.label('memory_size_bytes'),
            'Memory size in bytes.',
            value=convert(body, 'total')
        )]
        return metrics


class StorageCollector(BaseCollector):

    def update(self):
        body = self.endpoint.get('/statistics/systems/storage.json?timespan=1d')
        if not body:
            return []
        metrics = []
        metric = GaugeMetricFamily(
            self.label('partition_free_bytes'),
            documentation='Partition free space in bytes',
            labels=['partition'],
        )
        metrics.append(metric)
        for partition in ['boot', 'root']:
            metric.add_metric([partition], convert(body[partition], 'avail'))
        metric = GaugeMetricFamily(
            self.label('partition_used_bytes'),
            documentation='Partition used space in bytes',
            labels=['partition'],
        )
        metrics.append(metric)
        for partition in ['boot', 'root']:
            metric.add_metric([partition], convert(body[partition], 'used'))
        metric = GaugeMetricFamily(
            self.label('partition_size_bytes'),
            'Partition size in bytes.',
            labels=['partition'],
        )
        metrics.append(metric)
        for partition in ['root', 'boot']:
            metric.add_metric([partition], convert(body[partition], 'size'))
        return metrics


class InterfaceThroughputCollector(BaseCollector):
    SET_RE = re.compile(r'(.+)(in|out)bytes')

    def __init__(self, endpoint, facts, interfaces=r'.*', **kwargs):
        super().__init__(endpoint, facts, **kwargs)
        self.interfaces = interfaces

    def update(self):
        metrics = []
        directions = {'in': {}, 'out': {}}
        for interface in self.matches('interfaces', self.interfaces):
            interface = str(interface)
            body = self.endpoint.get(
                f'/analysis/interface_statistics.json?prefix={interface}&timespan=1d'
            )
            if not body:
                continue
            for entry in body['items']:
                timestamp = entry['x']
                # Interface statistics refreshed every 60 seconds, so double this for the last entry to be valid
                if stale(timestamp, age=120):
                    continue
                group, rate = entry['set'], entry['y']
                match = self.SET_RE.match(group)
                if not match:
                    continue
                ethernet, direction = match.groups()
                directions[direction][group] = [interface, ethernet, rate]
        for direction in ['in', 'out']:
            if not directions[direction]:
                continue
            metric = GaugeMetricFamily(
                self.label(f'interface_throughput_{direction}_bytes'),
                f'The average number of {direction}bound bytes per second recorded for an interface.',
                labels=['interface', 'ethernet'],
            )
            metrics.append(metric)
            for interface, ethernet, rate in directions[direction].values():
                metric.add_metric([interface, ethernet], rate)
        return metrics


class ProxyHealthCollector(BaseCollector):
    def update(self):
        body = self.endpoint.get('/wga/widgets/health.json')
        if not body:
            return []
        metrics = []
        metric = GaugeMetricFamily(
            self.label('proxy_health'),
            'Summary of reverse proxy health',
            labels=['proxy']
        )
        metrics.append(metric)
        for entry in body['items']:
            metric.add_metric([entry['name']], entry['health'])
        metric = GaugeMetricFamily(
            self.label('junction_health'),
            'Summary of reverse proxy junction health',
            labels=['proxy', 'junction'],
        )
        metrics.append(metric)
        for entry in body['items']:
            proxy = entry['name']
            for junction in entry.get('children', []):
                label = junction['label']
                if label[0] != '/':
                    label = '@' + label
                metric.add_metric([proxy, label], junction['health'])
        return metrics


class BucketCollector(BaseCollector):
    @staticmethod
    def stale(timestamp, age=60 * 10):
        ic(timestamp, age)
        return stale(timestamp, age)

    @staticmethod
    def date(offset=2100):  # 35 minutes
        now = datetime.now()
        date = now - timedelta(seconds=offset)
        return int(date.timestamp())


class ProxyThroughputCollector(BucketCollector):
    def __init__(self, endpoint, facts, proxies='.*', **kwargs):
        super().__init__(endpoint, facts, **kwargs)
        self.proxies = proxies

    def update(self):
        metrics = []
        metric = GaugeMetricFamily(
            self.label(f'proxy_throughput'),
            f'Latest summary of reverse proxy throughput records',
            labels=['proxy']
        )
        duration = 3600
        date = self.date()
        metrics.append(metric)
        for proxy in self.matches('proxies', self.proxies):
            body = self.endpoint.get(
                f'/analysis/reverse_proxy_traffic/throughput/{proxy}?duration={duration}&date={date}'
            )
            count = 0
            ic(proxy, body)
            if body:
                record = body[-1]
                timestamp = int(record['t'])
                if not self.stale(timestamp):
                    ic(timestamp, readable(timestamp), count)
                    count = record['h']
            metric.add_metric([proxy], count)
        return [metric]


class ProxyTrafficCollector(BucketCollector):
    def __init__(self, endpoint, facts, aspects=None, proxies='.*', **kwargs):
        super().__init__(endpoint, facts, **kwargs)
        if aspects is None:
            aspects = ['junction', 'useragent']
        self.proxies = proxies
        self.aspects = aspects

    def update(self):
        duration = 3600
        date = self.date()
        metrics = []
        for aspect in self.aspects:
            metric = GaugeMetricFamily(
                self.label(f'proxy_{aspect}_traffic'),
                f'Latest summary of reverse proxy traffic count by {aspect}',
                labels=['proxy', aspect]
            )
            metrics.append(metric)
            results = {}
            for proxy in self.matches('proxies', self.proxies):
                results[proxy] = {}
                body = self.endpoint.get(
                    f'/analysis/reverse_proxy_traffic/traffic/instance/{proxy}/'
                    f'?date={date}&duration={duration}&aspect={aspect}',
                    headers={'Range': 'items=0-24'}
                )
                ic(body)
                if not body:
                    continue
                for record in body:
                    timestamp = int(record['t'])
                    if stale(timestamp, 1200):
                        continue
                    count = record['h']
                    label = record['a']
                    if aspect == 'junction' and label[0] != '/':
                        label = '@' + label
                    ic(proxy, label, count)
                    results[proxy][label] = count
            if results:
                for proxy, values in results.items():
                    for label, count in values.items():
                        metric.add_metric([proxy, label], count)
        return metrics


class JunctionAverageResponseTimeCollector(BucketCollector):

    def __init__(self, endpoint, facts, proxies=r'.*', **kwargs):
        super().__init__(endpoint, facts, **kwargs)
        self.proxies = proxies

    def update(self):
        duration = 3600
        date = self.date()
        metrics, results = [], []
        for proxy in self.matches('proxies', self.proxies):
            body = self.endpoint.get(
                f'/analysis/reverse_proxy_traffic/reqtime?duration={duration}&date={date}&instance={proxy}'
            )
            ic(proxy, body)
            if not body:
                continue
            if 'records' not in body:
                continue
            if isinstance(body.get('records', None), dict):
                body['records'] = [
                    body['records'],
                ]
            for records in body.get('records', []):
                if records.get('records', None) is None:
                    continue
                junction, records = records['junction'], records['records']
                if junction[0] != '/':
                    junction = '@' + junction
                if isinstance(records, dict):
                    records = [
                        records,
                    ]
                results.append([proxy, junction, records])
        metric = GaugeMetricFamily(
            self.label('junction_average_response_time'),
            'Latest summary of reverse proxy junction response time in seconds',
            labels=['proxy', 'junction']
        )
        metrics.append(metric)
        for proxy, junction, records in results:
            record = records[-1]
            if stale(int(record['t']), 600):
                continue
            metric.add_metric([proxy, junction], float(record['r']))
        return metrics


class UptimeCollector(BaseCollector):

    def update(self):
        metrics = []
        now = time.time()
        body = self.endpoint.get('/firmware_settings/')
        if body:
            for partition in body:
                if not partition.get('active', False):
                    continue
                uptime = int(now - int(partition['last_boot']))
                uptime = max(uptime, 0)
                metrics.append(
                    CounterMetricFamily(
                        self.label('appliance_uptime_seconds'),
                        'Appliance uptime in seconds',
                        value=uptime
                    )
                )
        body = self.endpoint.get('/mga/runtime_profile/v1')
        if body:
            now = time.time()
            uptime = int(now - int(body['last_start']))
            uptime = max(uptime, 0)
            metrics.append(
                CounterMetricFamily(
                    self.label('runtime_uptime_seconds'),
                    'Runtime uptime in seconds', value=uptime
                )
            )
        return metrics


__all__ = [
    'CpuCollector',
    'MemoryCollector',
    'InterfaceThroughputCollector',
    'UptimeCollector',
    'StorageCollector',
    'ProxyHealthCollector',
    'ProxyThroughputCollector',
    'ProxyTrafficCollector',
    'JunctionAverageResponseTimeCollector',
    'ConsoleFactCollector',
]
