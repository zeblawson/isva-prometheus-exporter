import logging
import sys

from icecream import ic, install
from logging import Formatter, StreamHandler
from pathlib import Path

NAME = Path(__file__).stem

DEFAULT_FORMATTER = Formatter('[%(asctime)s] %(levelname)s %(message)s')


def prepare(debug=False, filename=None):
    install()  # Global icecream
    if filename:
        handler = logging.FileHandler(filename)
    else:
        handler = StreamHandler(sys.stdout)
    if debug:
        level = logging.DEBUG
    else:
        level = logging.INFO
    handler.setFormatter(DEFAULT_FORMATTER)
    logger.setLevel(level)
    logger.addHandler(handler)
    ic.configureOutput(outputFunction=logger.debug)


logger = logging.getLogger(NAME)
