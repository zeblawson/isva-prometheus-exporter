import yaml

from cerberus import Validator
from collector import *
from collector.console import *
from collector.logging import *
from collector.pd import *
from collector.runtime import *

CONSOLE_COLLECTORS = {
    'uptime': UptimeCollector,
    'cpu': CpuCollector,
    'memory': MemoryCollector,
    'storage': StorageCollector,
    'interface-throughput': InterfaceThroughputCollector,
    'proxy-health': ProxyHealthCollector,
    'proxy-traffic': ProxyTrafficCollector,
    'proxy-throughput': ProxyThroughputCollector,
    'junction-average-response-time': JunctionAverageResponseTimeCollector,
}

RUNTIME_COLLECTORS = {
    'runtime-jvm': RuntimeJvmCollector,
    'runtime-threads': RuntimeThreadsCollector,
    'runtime-database': RuntimeDatabaseCollector,
}

PD_COLLECTORS = {'pd-stats': PdStatsCollector}

COMMON_PARAMETERS = {
    'enabled': {'type': 'boolean'},
    'caching': {'type': 'boolean'}
}

SCHEMA = {
    'uptime': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'cpu': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'memory': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'storage': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'interface-throughput': {
        'type': 'dict',
        'nullable': True,
        'schema': {
            'interfaces': {'anyof_type': ['string', 'list']},
            'enabled': {'type': 'boolean'},
            'caching': {'type': 'boolean'}
        }
    },
    'proxy-health': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'proxy-throughput': {
        'type': 'dict',
        'nullable': True,
        'schema': {
            'proxies': {'anyof_type': ['string', 'list']},
            'enabled': {'type': 'boolean'},
            'caching': {'type': 'boolean'}
        }
    },
    'proxy-traffic': {
        'type': 'dict',
        'nullable': True,
        'schema': {
            'proxies': {'anyof_type': ['string', 'list']},
            'enabled': {'type': 'boolean'},
            'caching': {'type': 'boolean'},
            'aspects': {'type': 'list', 'allowed': ['junction', 'useragent']}
        }
    },
    'junction-average-response-time': {
        'type': 'dict',
        'nullable': True,
        'schema': {
            'proxies': {'anyof_type': ['string', 'list']},
            'enabled': {'type': 'boolean'},
            'caching': {'type': 'boolean'}
        }
    },
    'runtime-database': {
        'type': 'dict',
        'nullable': True,
        'schema': {
            'databases': {'type': 'list', 'allowed': ['hvdb', 'config']},
            'enabled': {'type': 'boolean'},
            'caching': {'type': 'boolean'}
        }
    },
    'runtime-jvm': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'runtime-threads': {'type': 'dict', 'nullable': True, 'schema': COMMON_PARAMETERS},
    'pd-stats': {
        'type': 'dict',
        'nullable': True,
        'schema': {
            'servers': {'anyof_type': ['string', 'list']},
            'components': {'anyof_type': ['string', 'list']},
            'enabled': {'type': 'boolean'},
            'caching': {'type': 'boolean'}
        }
    },
}


class YamlValidationError(Exception):
    def __init__(self, errors):
        super().__init__(self.__class__.__name__)
        self.errors = errors


def validate(data):
    ic()
    ic(data)
    logger.debug(ic())
    validator = Validator()
    validator.validate(data, SCHEMA)
    if validator.errors:
        raise YamlValidationError(validator.errors)


def load(path, console=None, runtime=None, pd=None):
    collectors, data = [], {}
    if path.is_file():
        with open(path, 'r', encoding='utf-8') as handle:
            data = yaml.safe_load(handle)
    validate(data)
    facts = {}
    collectors.append(ScrapeTimeCollector())
    if console:
        collectors.append(UpDownCollector(console, facts, 'console'))
        collectors.append(ConsoleFactCollector(console, facts))
    if runtime:
        collectors.append(UpDownCollector(runtime, facts, 'runtime'))
    if pd:
        collectors.append(PdFactCollector(console, pd, facts))
    for key, parameters in data.items():
        if parameters is None:
            parameters = {}
        if parameters.get('enabled', True) is False:
            continue
        if console and key in CONSOLE_COLLECTORS:
            collectors.append(CONSOLE_COLLECTORS[key](console, facts, **parameters))
        elif runtime and key in RUNTIME_COLLECTORS:
            collectors.append(RUNTIME_COLLECTORS[key](runtime, facts, **parameters))
        elif console and pd and key in PD_COLLECTORS:
            collectors.append(PD_COLLECTORS[key](console, pd, facts, **parameters))
    collectors.append(ScrapeTimeCollector())
    return collectors


__all__ = ('load', 'YamlValidationError')
