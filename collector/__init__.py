import re
import time

import requests
import threading

from collections import defaultdict
from datetime import datetime
from icecream import ic
from prometheus_client.metrics_core import GaugeMetricFamily
from urllib3.exceptions import InsecureRequestWarning
from .logging import logger


def convert(data, key):
    return float(data[key]) * 1024 * 1024


def readable(timestamp):
    return datetime.fromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S')


def stale(timestamp, age):
    now = datetime.now().timestamp()
    ic(timestamp, readable(timestamp), age, now, readable(now), now - timestamp)
    return now - timestamp > age


def matches(criteria, candidates):
    ic(criteria, candidates)
    if isinstance(criteria, list):
        return list(set(criteria).intersection(set(candidates)))
    elif isinstance(criteria, str):
        expression = re.compile(criteria)
        return list(filter(lambda candidate: expression.match(candidate), candidates))
    return candidates


class RequiredFactException(Exception):
    def __init__(self, key):
        super().__init__(f'{self.__class__.__name__}({key})')


class Endpoint:
    def __init__(
            self, address, username=None, password=None, verify=False, timeout=None
    ):
        if address[-1] == '/':
            address = address[:-1]
        self.address = address
        self.session = requests.Session()
        if username and password:
            self.session.auth = (username, password)
        if not verify:
            requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        self.session.verify = verify
        self.timeout = timeout
        self.online = False

    @property
    def offline(self):
        return not self.online

    def request(self, method, path, data=None, timeout=None, headers=None):
        ic(method, path, data, timeout, headers)
        if headers is None:
            headers = {}
        url = f'{self.address}{path}'
        try:
            headers.update({'Accept': 'application/json', 'Content-Type': 'application/json'})
            response = self.session.request(
                method, url, json=data, timeout=timeout if timeout else self.timeout, headers=headers
            )
            body = None
            logger.info(ic.format(url, response))
            if response.status_code == 200:  # 2XX range
                body = response.json()
                ic(method, url, data, response)
            else:
                ic(method, url, data, response, body)
            return body
        except Exception as error:
            ic(error)

    def get(self, path, **kwargs):
        return self.request('GET', path, **kwargs)

    def post(self, path, data, **kwargs):
        return self.request('POST', path, data=data, **kwargs)

    def ping(self):
        ic()
        url = f'{self.address}/'
        try:
            response = self.session.get(url, timeout=2)
            ic(response)
            return response.status_code // 100 == 2
        except Exception as error:
            ic(error)
            return False

    def __repr__(self):
        return f'{self.__class__.__name__}({self.address})'


class BaseCollector(object):
    def __init__(self, endpoint, facts, enabled=True, caching=False):
        self.endpoint = endpoint
        self.facts = facts
        self.enabled = bool(enabled)
        self.caching = caching
        if caching:
            self.thread = None
            self.cached = []  # This will be shared amongst threads

    @staticmethod
    def label(description):
        return f'isva_{description}'

    def matches(self, key, criteria=None):
        ic(key, criteria)
        return matches(criteria, self.facts.get(key, []))

    def skip(self):
        ic()
        ic(self.endpoint, self.endpoint.offline, self.enabled)
        return self.endpoint.offline or not self.enabled

    def cache(self):
        ic()
        self.cached = self.update()

    def collect(self):
        ic()
        ic(self, self.skip(), self.caching)
        start = time.time()
        metrics = []
        if self.skip() is False:
            if self.caching:
                ic(self.thread)
                if self.thread is None or self.thread.is_alive() is False:
                    ic()
                    self.thread = threading.Thread(target=self.cache)
                    self.thread.start()
                metrics = self.cached
            else:
                metrics = self.update()
        ic(metrics)
        execution = time.time() - start
        logger.info(ic.format(execution) + ' seconds')
        return metrics

    def update(self):
        ic()
        raise NotImplementedError

    def __repr__(self):
        return self.__class__.__name__

    __str__ = __repr__


class ScrapeTimeCollector(BaseCollector):
    threads = defaultdict()

    def __init__(self):
        super().__init__(None, None)

    def update(self):
        pass

    def collect(self):
        now = datetime.now().timestamp()
        if id in self.threads:
            timestamp = self.threads.pop(id)
            delta = now - timestamp
            metric = GaugeMetricFamily(
                self.label('total_collection_time'),
                'Total time exporter take for this scrape',
                value=delta
            )
            return [metric]
        else:
            self.threads[id] = now
            ic(now)
        return []


class UpDownCollector(BaseCollector):
    def __init__(self, appliance, endpoint, name):
        super().__init__(appliance, endpoint)
        self.name = name
        self.enabled = True

    def update(self):
        pass

    def collect(self):
        self.endpoint.online = self.endpoint.ping()
        ic(self.__class__.__name__, self.endpoint.online)
        metric = GaugeMetricFamily(
            self.label(f'{self.name}_up'),
            f'Indicates whether the {self.name} is up or down.',
            self.endpoint.online
        )
        return [metric]


__all__ = (
    'convert',
    'matches',
    'readable',
    'stale',
    'UpDownCollector',
    'Endpoint',
    'BaseCollector',
    'RequiredFactException',
    'ScrapeTimeCollector'
)
