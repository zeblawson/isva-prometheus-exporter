__title__ = 'isva-prometheus-exporter'
__license__ = 'MIT'
__version__ = '1.0.0'
__author__ = 'Zeb Lawson'
__contact__ = 'zeblawson@gmail.com'
__url__ = 'https://gitlab.com/zeblawson/isva-prometheus-exporter/'
__description__ = (
    'A Prometheus monitoring agent exporter for IBM© Security Verify Access appliances, ',
    'previously known as IBM© Security Access Manager.'
)
